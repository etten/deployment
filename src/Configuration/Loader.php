<?php

/**
 * Copyright © 2016 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Deployment\Configuration;

interface Loader
{

	public function validate($data): bool;

	public function load($data): array;

}
