<?php

/**
 * Copyright © 2016 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Deployment\Configuration;

use Etten\Deployment\Exception;

class LoaderStack implements Loader
{

	/** @var Loader[] */
	private $loaders = [];

	public function add(Loader $loader)
	{
		$this->loaders[] = $loader;
	}

	public function validate($data): bool
	{
		return $this->findLoader($data) instanceof Loader;
	}

	public function load($data): array
	{
		$loader = $this->findLoader($data);
		if ($loader) {
			return $loader->load($data);
		}

		throw new Exception('Cannot load a configuration.');
	}

	/**
	 * @param mixed $data
	 * @return Loader|null
	 */
	private function findLoader($data)
	{
		foreach ($this->loaders as $loader) {
			if ($loader->validate($data)) {
				return $loader;
			}
		}

		return NULL;
	}

}
