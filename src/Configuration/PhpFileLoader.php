<?php

/**
 * Copyright © 2016 Jaroslav Hranička <hranicka@outlook.com>
 */

namespace Etten\Deployment\Configuration;

use Etten\Deployment\Exception;

class PhpFileLoader implements Loader
{

	public function validate($data): bool
	{
		return is_file($data) && substr($data, -4) === '.php';
	}

	public function load($data): array
	{
		if ($this->validate($data)) {
			return require $data;
		} else {
			throw new Exception('Cannot load a configuration.');
		}
	}

}
